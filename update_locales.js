// name   : update_locales.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3

/* globals require */
const fs = require('fs')
const prefix = 'html_locales/'
let locales = []
fs.readdirSync(prefix).forEach(file => { locales.push(file) })
const not_locales = ['template.json', 'black_list.json', 'update_black_list']
locales = locales.filter(e => ! not_locales.includes(e))
const tpl_json = JSON.parse(fs.readFileSync(`${prefix}template.json`, 'utf8'))
const tpl_list = Object.keys(tpl_json)
for (const loc of locales) {
	const l_json = JSON.parse(fs.readFileSync(`${prefix}${loc}`, 'utf8'))
	const l_list = Object.keys(l_json)
	const lost_keys = l_list.filter(e => ! tpl_list.includes(e))
	const new_json = {}
	for (const key of tpl_list)
		new_json[key] = l_json[key] || tpl_json[key]
	if (lost_keys.length > 0) {
		const lost_keys_json = {}
		for (const key of lost_keys)
			lost_keys_json[key] = l_json[key]
		const dt = new Date().toISOString().replace('T','_').replace(':','h').split(':')[0]
		fs.writeFileSync(`lost_keys_${dt}_${loc}`, JSON.stringify(lost_keys_json, null, 2))
	}
	fs.writeFileSync(`${prefix}${loc}`, JSON.stringify(new_json, null, '\t'))
}
