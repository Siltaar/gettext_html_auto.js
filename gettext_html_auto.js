// name   : gettext_html_auto.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3
/* globals document NodeFilter fetch console */

import i18n from './gettext.esm.min.js'

const prefix = '/html_locales/'

/**
 *
 */
export function walk_text_nodes(apply_on_nodes){
	const walk = document.createTreeWalker(document, NodeFilter.SHOW_TEXT, { acceptNode:
		(node) => { if(! /^\s*$/.test(node.data)) return NodeFilter.FILTER_ACCEPT}}, false)
	let n
	while(n = walk.nextNode()) apply_on_nodes(n)  // assumed assign condition
}
/**
 *
 */
export function query_attr_nodes(attrs, apply_on_nodes){
	for (const attr of attrs)
		for (const elt of document.querySelectorAll(`[${attr}]`))
			apply_on_nodes(elt, attr)
}
/**
 * Replaces all text nodes and attributes (title, alt, href, placeholder) by translations in the
 * current web page
 * @param {string} wanted_locale Which locale to apply
 * @param {string} default_locale The default locale of your code (to return faster when your
 * wanted locale is the default one
 * @returns {Object} gettext_i18n Object allowing to programatically fetch translaction keys
 */
export async function gettext_html_auto(wanted_locale, default_locale='en') {
	const gettext_i18n = i18n()
	gettext_i18n.setLocale(wanted_locale)
	wanted_locale = wanted_locale.slice(0, 2)
	if (wanted_locale == default_locale)
		return gettext_i18n
	try {
		let locale_json = await fetch(`${prefix}${wanted_locale}.json`)
		locale_json = await locale_json.json()
		gettext_i18n.loadJSON(locale_json, 'messages')
	} catch (exc) {
		console.error(`Can't load: ${prefix}${wanted_locale}.json`)
		console.error(exc)
		return gettext_i18n
	}
	try {
		walk_text_nodes((n) => { n.data = gettext_i18n.gettext(n.data) })
		query_attr_nodes(['title', 'alt', 'href', 'placeholder'],
			(n, attr) => { n[attr] = gettext_i18n.gettext(n[attr]) })
	} catch (exc) {
		console.error(exc)
	}
	return gettext_i18n
}
/**
 * Extracts all the text nodes and attributes (title, alt, href, placeholder) from the current
 * web page to create a `template.json` file with all the translation keys. Avoids collecting
 * non HTTPS links (to avoid collecting local 127.0.0.1 relative links.
 * @param {string} locale The default locale of your code
 * @param {number} max_string_size Only string smaller than this limit will be collected
 */
export async function xgettext_html(locale='en', max_string_size=500){
	let tpl_json = {}
	let black_list = []
	try {
		const tpl_file = await fetch(`${prefix}template.json`)
		tpl_json = await tpl_file.json()
		const black_file = await fetch(`${prefix}black_list.json`)
		black_list = Object.keys(await black_file.json())
	} catch(exc) {
		console.error(exc)
		console.error(`template.json or black_list.json not in ${prefix}`)
		return
	}
	const collected_keys = {
		'': {
			'language': locale,
			'plural-forms': 'nplurals=2; plural=(n!=1);'
		},
	}
	let raw_keys = {}
	walk_text_nodes( (n) => { raw_keys[n.data] = '' })
	query_attr_nodes(['title', 'alt', 'placeholder'], // for button names, use <button>name</but…
		(n, attr) => {
			if (typeof(n[attr]) == 'undefined')
				console.error('Undefined string in', n, 'for attribute', attr)
			raw_keys[n[attr]] = ''
		})
	query_attr_nodes(['href'],
		(n, attr) => {
			if(n[attr].startsWith('https'))
				raw_keys[n[attr]] = ''
		})
	const tpl_keys = Object.keys(tpl_json)
	for (const key of tpl_keys)
		raw_keys[key] = tpl_json[key]
	raw_keys = Object.keys(raw_keys).filter(e => ! black_list.includes(e))
	raw_keys = raw_keys.filter(e => max_string_size > e.length)
	const new_keys = raw_keys.filter(e => ! tpl_keys.includes(e))
	if (new_keys.length) {
		console.info('New string detected', new_keys)
		for (const key of raw_keys)
			collected_keys[key] = ''
		const a_str = JSON.stringify(collected_keys, null, '\t')
		const elt_a = document.createElement('a')
		elt_a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(a_str)}`
		elt_a.download='template.json'
		elt_a.click()
	} else {
		console.info('No new string detected.')
	}
}
